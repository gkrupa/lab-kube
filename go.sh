#!/usr/bin/env bash

ALL_NODES=(k8s-master k8s-node-1 k8s-node-2 k8s-node-3)
for NODE in ${ALL_NODES[*]}; do
	KUBEADM=$(ssh krupagj@${NODE} "which kubeadm")
	if [[ "${KUBEADM}" == "" ]]; then
		echo "Installing packages on $NODE (${NODE})"
		scp install.sh krupagj@${NODE}:/home/krupagj/install.sh
		ssh krupagj@${NODE} sudo /home/krupagj/install.sh

	else
		echo "Packages already installed on $NODE"
	fi
done

MASTER_PUBLIC_IP=192.168.1.70
MASTER_PUBLIC_DNS=k8s-master.localdomain

TOKEN=$(ssh krupagj@${MASTER_PUBLIC_IP} "sudo kubeadm token create --ttl 1h")

ssh krupagj@${MASTER_PUBLIC_IP} "sudo kubeadm reset"
ssh krupagj@${MASTER_PUBLIC_IP} "sudo kubeadm init \
	--apiserver-advertise-address=${MASTER_PUBLIC_IP} \
	--apiserver-cert-extra-sans=${MASTER_PUBLIC_IP},${MASTER_PUBLIC_DNS} \
	--token=${TOKEN}"

#--pod-network-cidr=10.244.0.0/16

ssh krupagj@${MASTER_PUBLIC_IP} sudo cp /etc/kubernetes/admin.conf /home/krupagj/admin.conf
ssh krupagj@${MASTER_PUBLIC_IP} sudo chown krupagj:krupagj /home/krupagj/admin.conf
scp krupagj@${MASTER_PUBLIC_IP}:/home/krupagj/admin.conf admin.conf

kubectl --kubeconfig=admin.conf apply -f dns-configmap.yaml

WORKER_NODES=(k8s-node-1 k8s-node-2 k8s-node-3)
for NODE in ${WORKER_NODES[*]}; do
	ssh krupagj@${NODE} "sudo kubeadm reset"
	ssh krupagj@${NODE} "sudo kubeadm join --token=${TOKEN} ${MASTER_PUBLIC_DNS}:6443 --discovery-token-unsafe-skip-ca-verification"
done

export kubever=$(kubectl --kubeconfig=admin.conf version | base64 | tr -d '\n')
kubectl --kubeconfig=admin.conf apply -f "https://cloud.weave.works/k8s/net?k8s-version=${kubever}"

while [[ "$(kubectl --kubeconfig=admin.conf get nodes | grep NotReady)" != "" ]]; do
	echo Waiting for nodes to be ready
	sleep 2
done

kubectl --kubeconfig=admin.conf create serviceaccount --namespace kube-system tiller
kubectl --kubeconfig=admin.conf create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
KUBECONFIG=admin.conf helm init --service-account tiller

while [[ "$(kubectl --kubeconfig=admin.conf get pods -n kube-system | grep tiller | awk '{print $3}')" != "Running" ]]; do
	echo Waiting for Tiller pod to be ready
	sleep 2
done
sleep 10

kubectl --kubeconfig=admin.conf create ns ingress
kubectl --kubeconfig=admin.conf create clusterrolebinding default-cluster-rule --clusterrole=cluster-admin --serviceaccount=ingress:default
KUBECONFIG=admin.conf helm install stable/nginx-ingress \
	--name ingress \
    --namespace ingress \
	--set controller.hostNetwork=true \
	--set controller.kind=DaemonSet

