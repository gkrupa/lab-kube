#!/usr/bin/env bash

kubectl --kubeconfig=admin.conf delete -f rook-cluster.yaml
kubectl --kubeconfig=admin.conf delete -f rook-operator.yaml

while [[ ! -z "$(kubectl --kubeconfig=admin.conf get ns | grep rook)" ]]; do
	echo "Waiting for rook to terminate"
	sleep 1
done

ALL_NODES=(k8s-node-1 k8s-node-2 k8s-node-3)
for NODE in ${ALL_NODES[*]}; do
	ssh krupagj@${NODE} sudo rm -rf /opt/rook/data
	ssh krupagj@${NODE} sudo parted /dev/sdb rm 3
	ssh krupagj@${NODE} sudo parted /dev/sdb rm 2
	ssh krupagj@${NODE} sudo parted /dev/sdb rm 1
done

kubectl --kubeconfig=admin.conf create -f rook-operator.yaml
kubectl --kubeconfig=admin.conf create -f rook-cluster.yaml
kubectl --kubeconfig=admin.conf create -f rook-tools.yaml

