# Lab Kubernetes Cluster Configuration

This project installs a Kubernetes cluster using kubadm and Helm that includes an Ingress as well as persistent storage using Rook and Ceph on any available non-root volumes.  An external load balancer (e.g. Traefik) should be used to support access to all worker nodes and provide SSL certificates.

This cluster is not production ready and is intended for cloud labs.  It includes only a single master/API server and any number of worker nodes.

Running the script again will wipe and reset the cluster.

## Requirements

  1. A home lab with multiple hosts running Ubuntu 16.04 with fixed IPs and sudo/keyless SSH enabled (see enable_sudo_fixed_ip.txt)
  2. Local DNS for the machines with hostnames k8s-master, k8s-node-1, k8s-node-2, etc.
  3. kubectl (latest version) on your ${PATH}

## Instructions

  1. Search and replace krupagj with your username
  2. Run go.sh
  3. Run add-rook.sh (if you want GlusterFS storage)
